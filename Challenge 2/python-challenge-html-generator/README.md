# Challenge de python

Par équipes de 3, vous devrez vous répartir les tâches pour le challenge suivant : 

Vous avez à votre disposition un fichier JSON avec une liste de couleurs. 
Votre objectif, sera de représenter l'ensemble de ces couleurs, ainsi que leurs code couleur associées, au sein d'une page HTML. 

Vous devrez pour ce faire : 
 - Lire le fichier JSON
 - Définir en équipe la représentation graphique HTML attendue
 - Définir des fonctions à réaliser pour manipuler les données du JSON
 - Définir des fonctions qui génèrent du HTML
 - Sauvegarder le HTML généré au sein de fichiers statiques HTML
 - Définir des tests unitaires sur vos fonctions de manipulation de données JSON

Le rendu est attendu au sein d'un dépot gitlab.


