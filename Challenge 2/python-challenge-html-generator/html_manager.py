import os
import json
import sys




# BONUS Function which returns the number of colors contained in the json file.
# @param includeChildren : boolean
# return integer; number of colors
def getTotal(filename, includeChildren):
	with open(filename) as json_colors:
		# stock file in colors_item
   		colors_item = json.load(json_colors)
	# create array to insert values from for loop
	array = []
	# for loop to browse colors_item & append requested values
	for value in colors_item:
		array.append(value['name'])
	
	

	return (len(array))

	


# Function which reads a JSON file
# And returns a structured array.
# @param string filename
# return array

def jsonToArray(filename):
	# define var = json file name

	# open json file & read it, close it when done
	with open(filename) as json_colors:
		# stock file in colors_item
   		colors_item = json.load(json_colors)
	# create array to insert values from for loop
	array = []
	# for loop to browse colors_item & append requested values
	for value in colors_item:
		array.append(value['name'])
		array.append(value['hex'])
	return(array)

	



def arrayToHtml(colors):
	htmldebut = """<!DOCTYPE html> <html> <head title="Monsite"> <meta charset="utf-8"></head> <body>"""


	divcolor = """<div style="background-color: """
	enddivcolor = """ ";><h1>"""
	middleColor =  """</h1></div>"""
	htmlfin = """</body> </html>"""
	i = 0
	while i<len(colors):
		if i % 2 == 0:
			intermediaire2 = enddivcolor + colors[i] + middleColor
		if i % 2 == 1 :  
			intermediaire =  divcolor + colors[i]
			htmldebut = htmldebut + intermediaire + intermediaire2
		i=i+1

	HTML = htmldebut + htmlfin

	return HTML


# Save an HTML string to a given file.
# @param filename : path to output file
# @param htmlString : content to save in file
# return absolute path to filename



def htmlToFile(HTML, string):
	# TODO

	Html_file= open(string ,"w")
	Html_file.write(HTML)
	Html_file.close()

	
	return ('./' + string)
	

# BONUS function which returns the children of a node.
# @param string reprsenting a color name
# return array
def getChildren(nodeName):
	 with open('colors.json') as json_colors:
       colors_item = json.load(json_colors)

       childrensArray = []
       for value in colors_item
           if value['name'] == nodeName
               for values in value['children']
                   childrensArray.append(values['name'], values['hexa']) 

       return childrensArray;






htmlToFile(arrayToHtml(jsonToArray('colors.json')), 'coucou')

