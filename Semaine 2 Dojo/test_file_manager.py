import file_manager
import unittest

class FileManagerTest(unittest.TestCase):
    """FileManagerTest: Test case sur le parcours d'un fichier JSON."""

    def setUp(self):
        """Initialisation des tests."""
        self.file = 'film.JSON'

    def test_count_correct_file(self):
        """Open the JSON file and count the records"""
        count=file_manager.count('film.JSON')
        self.assertEqual(count, 16, "Invalid count of records in file.JSON")

    def test_count_empty_parameter(self):
        """No parameter given, None should be returned"""
        count=file_manager.count()
        self.assertEqual(count, None)

    def test_count_incorrect_file(self):
        """File is not JSON structure, should be skipped and None returned"""
        count=file_manager.count('Readme.md')
        self.assertEqual(count, None)

    def test_count_missing_file(self):
        """Non existing file should not crash and None returned"""
        count=file_manager.count('missing_file.JSON')
        self.assertEqual(count, None)

    def test_count_correct_file_new_extension(self):
        """Extension filter should not be case sensitive"""
        count=file_manager.count('film_2.json')
        self.assertEqual(count, 2)

    # def test_films_written_by(self):
    #   """Search for records written by Vince Gilligan"""
    #   films=file_manager.written_by(self.file, "Vince Gilligan")
    #   self.assertEqual(len(films), 1, "Invalid number of records")
    #   self.assertEqual(films[0]['Writer'], "Vince Gilligan", "Invalid record")
    #
    # def test_longest_title(self):
    #   """Search for the film having the longest title"""
    #   record=file_manager.longest_title(self.file)
    #   self.assertEqual(record['Title'], "Rogue One: A Star Wars Story", "Incorrect record returned")
    #
    # def test_best_rating(self):
    #   """Search for the film hacing the best imdbRating """
    #   record=file_manager.best_rating(self.file)
    #   self.assertEqual(record['Title'], "Game of Thrones", "There seems to be something even better")
    #
    # def test_latest_film(self):
    #   """Search for the record having the latest Year, return the film's title """
    #   film_name=file_manager.latest_film(self.file)
    #   self.assertEqual(film_name, "Assassin's Creed", "Invalid count of records in file.JSON")
    #
    #
    # def test_films_per_genre(self):
    #   """Search for records having as genre: Fantasy"""
    #   films=file_manager.find_per_genre(self.file, "Fantasy")
    #   self.assertEqual(len(films), 5, "Invalid number of records")
    #
    # def test_bonus_release_after_Nov_2015(self):
    #   """Search for the record having a release date >= Nov 2015 """
    #   records=file_manager.released_after(self.file, "30/10/2015")
    #   self.assertEqual(len(records), 4, "Invalid count of records in file.JSON")
