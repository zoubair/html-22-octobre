def count( filename = None):
	import os.path
	if filename == None:
		return None
	else:
		extension = filename[-5:].lower()
		
		if extension == ".json":
			existe = os.path.exists(filename)
			if existe:

				ouvrir = open(filename, "rt")
				tableau = ouvrir.read()

				resultat = len(tableau)
				ouvrir.close()


				return(resultat)
			else :
				return None
		else :
			return None
